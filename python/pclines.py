import cv2
import copy
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage.filters import maximum_filter
from scipy.ndimage.morphology import generate_binary_structure, binary_erosion

class image:
    debug = 0
    def __init__(self, path, debug=False):
        self.path = path 
        self.img = cv2.imread(self.path)
        #import pdb; pdb.set_trace();

    def gray(self):
        new_image = copy.deepcopy(self)
        new_image.path = new_image.path + " (gray)"
        new_image.img = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
        return new_image

    def edges(self):
        new_image = copy.deepcopy(self)
        new_image.path = new_image.path + " (edges)"
        gray = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
        new_image.img = cv2.Canny(gray, 50, 150, apertureSize = 3)
        return new_image

    def show(self):
        cv2.imshow(self.path, self.img)
        cv2.waitKey(0)

    def array(self):
        return self.img

    def invert(self):
        self.img = cv2.bitwise_not(self.img)

    def binarize(self):
        self.img = np.clip(0, 1, self.img)

    def get_dimensions(self):
        return np.array(self.img.shape)

    def __str__(self):
        out = "image object:\n"
        out += " |- path: " + self.path + "\n"

        dimensions = self.get_dimensions()
        out += " '- dimensions:\n"
        out += "     |- height: " + str(dimensions[0]) + "\n"
        out += "     '- width: " + str(dimensions[1]) + "\n"
        if len(dimensions) > 2:
            out += "     '- channels : " + str(dimensions[2])  + "\n"
        return out

def immaximas(image, thresh):
    """
    Takes an image and detect the peaks usingthe local maximum filter.
    Returns a boolean mask of the peaks (i.e. 1 when
    the pixel's value is the neighborhood maximum, 0 otherwise)
    """

    # define an 8-connected neighborhood
    #neighborhood = generate_binary_structure(2,2)
    neighborhood = np.full((7, 7), True)

    #apply the local maximum filter; all pixel of maximal value 
    #in their neighborhood are set to 1
    local_max = maximum_filter(image, footprint=neighborhood)==image
    #local_max is a mask that contains the peaks we are 
    #looking for, but also the background.
    #In order to isolate the peaks we must remove the background from the mask.

    #we create the mask of the background
    background = (image==0)

    #a little technicality: we must erode the background in order to 
    #successfully subtract it form local_max, otherwise a line will 
    #appear along the background border (artifact of the local maximum filter)
    eroded_background = binary_erosion(background, structure=neighborhood, border_value=1)

    #we obtain the final mask, containing only peaks, 
    #by removing the background from the local_max mask (xor operation)
    detected_peaks = local_max ^ eroded_background

    output = []
    r = 0
    for row in detected_peaks:
        c = 0
        for item in row:
            if item and image[r,c] <= thresh:
                output.append([r, c, image[r,c]])
            c += 1
        r += 1
    return output

def rasterizeTSspace(fid, X, Y, H):
    debug = 0
    if debug:
        print("[DEBUG]: entering rasterizeTSspace function\n")
        print("[DEBUG]: received arguments:")
        print("[DEBUG]: X:\n" + str(X))
        print("[DEBUG]: Y:\n" + str(Y))
        print("[DEBUG]: H:\n" + str(H))

    #H = rasterHalf(fid, X, Y, H, 1, 0);
    H = rasterHalf(fid, X, Y, H, 0, 0);

    # offset = (((H.shape[0])/2)-2)
    offset = (((H.shape[1])/2)-1)

    #H = rasterHalf(fid, Y, -X + 1, H, 2, offset);
    H = rasterHalf(fid, Y, (-X + 1), H, 1, offset);

    if debug:
        print("[DEBUG]: offset: " + str(offset))
        print("[DEBUG]: received H:\n" + str(H))
        print("[DEBUG]: exiting rasterizeTSspace function\n")
    return H

def rasterHalf(fid, X, Y, H, start, offset):
    # H has 300 rows and 600 columns (I guess)
    # X has 11799 rows and 1 column
    # Y has 11799 rows and 1 column
    # L has 11799 rows and 300 columns
    debug = 0
    if debug:
        print("[DEBUG]: entering rasterHalf function")
        print("[DEBUG]: received arguments:")
        print("[DEBUG]: start:" + str(start))
        print("[DEBUG]: offset: " + str(offset))
        print("[DEBUG]: X:")
        print("[DEBUG]: number of rows = " + str(X.shape[0]))
        print("[DEBUG]: number of columns = " + str(X.shape[1]))
        #print("[DEBUG]: X's items:\n" + str(X))
        print("[DEBUG]: Y:")
        print("[DEBUG]: number of rows = " + str(Y.shape[0]))
        print("[DEBUG]: number of columns = " + str(Y.shape[1]))
        #print("[DEBUG]: Y's items:\n" + str(Y))
        print("[DEBUG]: H:")
        print("[DEBUG]: number of rows = " + str(H.shape[0]))
        print("[DEBUG]: number of columns = " + str(H.shape[1]))
        #print("[DEBUG]: H's items:\n" + str(H))

    l = np.linspace(X.astype(int), Y.astype(int), int((H.shape[1]/2)-1))
    L = np.squeeze(l).T
    # or
    #L = l

    if debug:
        print("[DEBUG]: received L:")
        print("[DEBUG]: number of rows = " + str(L.shape[0]))
        print("[DEBUG]: number of columns = " + str(L.shape[1]))
        print("[DEBUG]: iterating over L's lines:")

    # subtracting 1 from L because in Python the first index is 0
    L -= 1

    # iterate over L lines
    for i in range(L.shape[0]):
        # subtracting 1 from (H.shape[0]/2) because in Python the first index is 0
        row = L[i] + ((H.shape[0]/2)-1)

        if debug:
            print("[DEBUG]: line " + str(i) + " of L:)\n" + str(row))
            print("[DEBUG]: iterating over current line:")
            print("[DEBUG]: starting at index: " + str(start))
            print("[DEBUG]: row.shape[0] = " + str(row.shape[0]))

        for j in range(start, row.shape[0]):
            x_index = round(row[j])
            y_index = (j + offset)

            if debug:
                print("[DEBUG]: remembering offset = " + str(offset))
                print("[DEBUG]: incrementing the value of H at position X = " + str(x_index) + " and Y = " + str(y_index))
            H[int(x_index) , int(y_index)] += 1;

            if debug:
                print("[DEBUG]: new value = ", str(H[int(x_index) , int(y_index)]))

    if debug:
        print("[DEBUG]: exiting rasterHalf function\n")

    return H

def findMaxima(fid, H, N, thresh):
    def get_first_item(thetuple):
        return thetuple[0]

    debug = 0
    if debug:
        print("[DEBUG]: entering findMaxima function\n")
        print("[DEBUG]: received arguments:")
        print("[DEBUG]: H:\n" + str(H))
        print("[DEBUG]: N:\n" + str(N))
        print("[DEBUG]: thresh:" + str(thresh))

    v = []
    u = []
    w = []
    for maximas in immaximas(H, thresh):
        #[v,u,w]
        v.append(maximas[0]) 
        u.append(maximas[1])
        w.append(maximas[2])

    if not w:
        if debug:
            print("[DEBUG]: exiting findMaxima prematuraly because w is empty\n")
        return None
    
    # add indexes and sort w in a descending order
    i = 0
    indexed_w = []
    for dv in w:
        indexed_w.append((dv, i))
        i += 1
    indexed_w.sort(key=get_first_item, reverse=True)
    # get the value-sorted w indexes list
    indexes = []
    for i in indexed_w:
        indexes.append(i[1])
    
    # sort u and v according to w sort
    # and truncate the result to the smallest size
    # between w and N
    if debug:
        print("[DEBUG]: u:\n" + str(u))
        print("[DEBUG]: v:\n" + str(v))
    count = min(len(indexed_w), N)
    nu = [u[i] for i in indexes[0:count]]
    nv = [v[i] for i in indexes[0:count]]

    if debug:
        print("[DEBUG]: count: " + str(count))
        print("[DEBUG]: u:\n" + str(u))
        print("[DEBUG]: v:\n" + str(v))
        print("[DEBUG]: exiting findMaxima function\n")
    #plt.plot(nu, marker="o", linestyle = 'None', color='b')
    #for a in range(nu:
    #plt.plot(nv, marker="o", linestyle = 'None', color='r')
    #plt.show()
    #input("press any ENTER to continue")
    return nv, nu

def lineParameters(fid, u, v, S):
    debug = 0
    if debug:
        print("[DEBUG]: entering lineParameters function\n")
        print("[DEBUG]: received arguments:\n")
        print("[DEBUG]: v\n" + str(v))
        print("[DEBUG]: u\n" + str(u))
        print("[DEBUG]: S\n" + str(S))

    d0 = (S[0]/2)
    v = [x - d0 for x in v]
    d1 = (S[1]/2)
    u = [d1 - x for x in u]

    if debug:
        print("[DEBUG]: d0\n" + str(d0))
        print("[DEBUG]: d1\n" + str(d1))
        print("[DEBUG]: new values for v and u:\n")
        print("[DEBUG]: v\n" + str(v))
        print("[DEBUG]: u\n" + str(u))

    # theta = atan2(d - abs(u), u);
    theta = [np.arctan(((d1 - abs(x))/x)) for x in u]

    # rho = (v.*d./(d - abs(u))).*sin(theta);
    rho = np.multiply(
            (np.multiply(
                v,
                (np.divide(
                    d1,
                    [d1 - abs(x) for x in u]
                    ))
                )
            ),
            np.sin(theta))

    if debug:
        print("[DEBUG]: theta:\n" + str(theta))
        print("[DEBUG]: rho:\n" + str(rho))
        print("[DEBUG]: exiting lineParameters function\n")

    return theta, rho

def pclines(fid, src, N):
    debug = 0
    if debug:
        print("[DEBUG]: entering pclines function\n")
        print("[DEBUG]: received arguments:")
        print("[DEBUG]: src:\n" + str(src))
        print("[DEBUG]: src as an array:\n" + str(src.array()))
        print("[DEBUG]: N:\n" + str(N))
    # y, x
    sz = src.get_dimensions()

    SpaceSize = [max(sz), max(sz)*2]
    H = np.zeros(SpaceSize)
    x, y = np.where(src.array() == 0)

    if debug:
        print("[DEBUG]: SpaceSize: " + str(SpaceSize))
        print("[DEBUG]: H:\n" + str(H))
        print("[DEBUG]: y:\n" + str(y))
        print("[DEBUG]: x:\n" + str(x))
        #print("[DEBUG]: zeros:\n" + str(zeros))

    x -= np.int64(sz[1]/2)
    y -= np.int64(sz[0]/2)

    H = rasterizeTSspace(fid, np.reshape(x, (-1,1)), np.reshape(y, (-1,1)), H);
    #cv2.imshow('H', H)
    #cv2.waitKey(0)

    # you must provide a 2-dimension matrix as src
    # otherwise the following line will panic
    thresh = (math.sqrt((np.matmul(sz[np.newaxis], sz[np.newaxis].T)).item())/4)

    v, u = findMaxima(fid, H, N, thresh)
    for i in range(len(v)):
        plt.plot([0, v[i]], [200, u[i]])
    plt.show()
    input("bla")
    
    theta, rho = lineParameters(fid, u, v, SpaceSize)

    if debug:
        print("[DEBUG]: adjusted y:\n" + str(y))
        print("[DEBUG]: adjusted x:\n" + str(x))
        print("[DEBUG]: thresh:\n" + str(thresh))
        print("[DEBUG]: exiting pclines function\n")

    return theta, rho

if __name__ == "__main__":

    # use it to get the complete arrays on debug messages
    np.set_printoptions(threshold=np.inf)

    img = image("/home/fschenkels/Downloads/sudoku.jpg")
    edges = img.edges()
    edges.invert()
    edges.binarize()

    theta, rho = pclines(None, edges, 10)

    print("[INFO]: theta:\n" + str(theta))
    print("[INFO]: rho:\n" + str(rho))

    for i in range(len(theta)):
        my_theta = theta[i]
        my_rho = rho[i]
        a = math.cos(my_theta)
        b = math.sin(my_theta)
        x0 = a * my_rho;
        y0 = b * my_rho;
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))

        print("[INFO]: p0 = [" + str(x1) + ", " + str(y1) + "], p1 = [" + str(x2) + ", " + str(y2) + "]")
        plt.plot([x1, y1], [x2, y2])

    plt.show()

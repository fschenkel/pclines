function [H, theta, rho] = pclines(src, N)
    S = size(src);
    SpaceSize = [max(S), max(S)*2];
    H = zeros(SpaceSize);
    
    [y,x] = find(src == 0);
    
    x -= (S(2)/2);
    y -= (S(1)/2);
 
    H = rasterizeTSspace(x(:), y(:), H);
    thresh = sqrt(S*S')/4;
    [u,v] = findMaxima(H, N, thresh);
    for i = 1:size(u)
        plot([0, u(i)], [200, v(i)]);
        hold on;
    end
    hold off;

    [theta, rho] = lineParameters(u,v,SpaceSize)
end

function [H] = rasterizeTSspace(X,Y,H)
    H = rasterHalf(X, Y, H, 1, 0);
    
    offset = size(H,2)/2-1;
    H = rasterHalf(Y, -X + 1, H, 2, offset);
end

function [H] = rasterHalf(X, Y, H, start, offset)
    L = linspace(X, Y, size(H,2)/2);
    
    size(L,1)
    
    for i = 1:size(L,1)
        row = L(i,:) + size(H,1)/2;
        
        for j = start:size(L,2)
            H(round(row(j)), j + offset) += 1;
        end
    end
end

function [u,v] = findMaxima(H, N, thresh)
  [v,u,w] = immaximas(H, 3, thresh);
    
  if (length(w) == 0)
    return;
  end
    
  [tmp,i] = sort(w, 'descend');
    
  count = min(length(w), N);
  u = u(i(1:count));
  v = v(i(1:count));
end

function [theta, rho] = lineParameters(u, v, S)
    d = S(2)/2;
    u = d - u;
    v = v - S(1)/2;
    
    theta = atan2(d - abs(u), u);
    rho = (v.*d./(d - abs(u))).*sin(theta);
end

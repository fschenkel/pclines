clear

filename = "octave.log";
fid = fopen (filename, "w");
# Do the actual I/O here…

im = imread('/home/fschenkels/Downloads/sudoku.jpg');

#fprintf(fid, "[DEBUG]: im:\n")
#fprintf(fid, "[DEBUG]: number of rows = %d\n", rows(im))
#fprintf(fid, "[DEBUG]: number of columns = %d\n", columns(im))
#fdisp(fid, im)

im=rgb2gray(im);

imshow(im);
%title('Imagem Original');

IB1 = edge(im, 'Canny');
%imshow(IB1);

#fprintf(fid, "[DEBUG]: im:\n")
#fprintf(fid, "[DEBUG]: number of rows = %d\n", rows(im))
#fprintf(fid, "[DEBUG]: number of columns = %d\n", columns(im))
#fdisp(fid, im)
#fprintf(fid, "[DEBUG]: IB1:\n")
#fprintf(fid, "[DEBUG]: number of rows = %d\n", rows(IB1))
#fprintf(fid, "[DEBUG]: number of columns = %d\n", columns(IB1))
#fdisp(fid, IB1)

[m, u, v] = pclines(1-IB1, 10);
%[m, u, v] = pclines(fid, 1-IB1, 10);

figure; hold on; axis equal;
axis([-150 150 -150 150]);

printf("[INFO]: u\n")
disp(u)
printf("[INFO]: v\n")
disp(v)
printf("[INFO]: m\n")
disp(m)


for i = 1:size(u)
  theta = u(i);
  rho = v(i);
  a = cos(theta);
  b = sin(theta);
  x0 = a*rho;
  y0 = b*rho;
  x1 = (x0 + 1000*(-b))
  y1 = (y0 + 1000*(a))
  x2 = (x0 - 1000*(-b))
  y2 = (y0 - 1000*(a))
  
  printf("[INFO]: p0 = [%d, %d], p1 = [%d, %d]", x1, y1, x2, y2)
  drawLine([x1 y1 x2 y2], 'color', 'b', 'linewidth', 1);    
end

fclose (fid);




%theta = 1.19382
%rho =   1.2625
figure; hold on; axis equal;
axis([-1000 1000 -1000 1000]);

for i = 1:size(u)
  theta = u(i);
  rho = v(i);
  a = cos(theta);
  b = sin(theta);
  x0 = a*rho;
  y0 = b*rho;
  x1 = (x0 + 1000*(-b))
  y1 = (y0 + 1000*(a))
  x2 = (x0 - 1000*(-b))
  y2 = (y0 - 1000*(a))
  
  drawLine([x1 y1 x2 y2], 'color', 'b', 'linewidth', 1);    
  %line ([x1 y1], [x2 y2], "linestyle", "-", "color", "b");
 end